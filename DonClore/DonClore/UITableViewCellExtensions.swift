//
//  UITableViewCellExtensions.swift
//  DonClore
//
//  Created by Don Clore on 8/27/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

extension UITableViewCell {
  func drawLowerBorder(_ rect: CGRect, color : UIColor, width : CGFloat) {
    
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    context.saveGState()
    defer {
      context.restoreGState()
    }
    let borderColor = color.cgColor
    let x = rect.origin.x
    let y = x + rect.height - width
    let cx = rect.width
    let cy = CGFloat(width)
    
    let r = CGRect(x: x, y: y, width: cx, height: cy)
    let path = UIBezierPath(rect: r)
    context.setStrokeColor(borderColor)
    path.lineWidth = width
    
    path.stroke()
  }
}

