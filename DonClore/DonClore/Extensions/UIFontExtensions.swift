//
//  UIFontExtensions.swift
//  DonClore
//
//  Created by Don Clore on 8/27/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

extension UIFont {
  
  class func preferredBoldFont(forTextStyle style: UIFontTextStyle) -> UIFont {
    return preferredFont(forTextStyle: style, withSymbolicTrait: .traitBold)
  }

  class func preferredItalicFont(forTextStyle style: UIFontTextStyle) -> UIFont {
    return preferredFont(forTextStyle: style, withSymbolicTrait: .traitItalic)
  }

  class func preferredFont(forTextStyle style: UIFontTextStyle,
    withSymbolicTrait trait : UIFontDescriptorSymbolicTraits) -> UIFont {
  
    let fontDesc = UIFontDescriptor.preferredFontDescriptor(withTextStyle: style)
    let traitDesc = fontDesc.withSymbolicTraits(trait)
    return UIFont(descriptor: traitDesc!, size: 0)
  }
}
