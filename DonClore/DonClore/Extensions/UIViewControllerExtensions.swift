//
//  UIViewControllerExtensions.swift
//  DonClore
//
//  Created by Don Clore on 8/27/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

extension UIViewController {
  func simpleErrorMessage(_ error: String, completion: (() -> Void)?  = nil) {
    let ac = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
    let ok = UIAlertAction(title: "OK", style: .default, handler: { _ in
      self.dismiss(animated: true, completion: completion)
    })
    ac.addAction(ok)
    present(ac, animated: true, completion: nil)
  }
  
  func fireUpVideo(name: String, callback: @escaping (VideoView) -> ()) {
    let videoRect = getVideoRect()
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
      guard let videoURL = Bundle.main.url(forResource: name, withExtension: "MOV") else {
        return
      }
      let videoURLPath = videoURL.absoluteString.replacingOccurrences(of: "file://", with: "")
      let fileExists =  FileManager.default.fileExists(atPath: videoURLPath)
      if false == fileExists {
        return
      }
      
      self.play(videoURL: videoURL, inRect: videoRect, callback: callback)
      
    })
  }
  
  func fireUpVideo(fromURL url : URL, callback: @escaping (VideoView) -> ()) {
    let videoRect = getVideoRect()
    
    play(videoURL: url, inRect: videoRect, callback: callback)
  }
  
  fileprivate func play(videoURL : URL, inRect videoRect : CGRect,
    callback: @escaping (VideoView) -> ()) {
    
    let videoView = VideoView(frame: videoRect)
    videoView.viewToShowIn = view
    callback(videoView)
    
    videoView.load(video: videoURL)
  }
  
  fileprivate func getVideoRect() -> CGRect {
    let videoWidth : CGFloat = view.frame.width / 2.0
    let videoHeight : CGFloat = videoWidth * (9.0 / 16.0)
    let videoX : CGFloat = videoWidth / 2.0
    let videoY : CGFloat = view.frame.height * 0.30
    
    let videoRect = CGRect(x: videoX, y: videoY, width: videoWidth, height: videoHeight)
    return videoRect
  }
}
