//
//  InflectedVC.swift
//  DonClore
//
//  Created by Don Clore on 8/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class InflectedVC: UIViewController {

  @IBOutlet var inflectedImageView: UIImageView!

  
  @IBOutlet var textField: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    textField.delegate = self
  }
}

extension InflectedVC : UITextFieldDelegate {
  func textFieldDidEndEditing(_ textField: UITextField) {
    guard let text = textField.text else {
      return
    }
    guard let sides = Int(text) else {
      return
    }
    print("sides = \(sides)")
    
    guard let image = pushGetImage(with: { (r : CGRect) in
      guard let b = UIBezierPath.inflectedShape(numberOfInflections: sides,
        percentInflection: 1.0) else {
        return
      }
      b.fit(to: r)
      b.fill(withColor: .blue)
      b.stroke(withWidth: 4.0, andColor: .black)
    }, andSize: inflectedImageView.bounds.size) else {
      return
    }
  
    inflectedImageView.image = image
    inflectedImageView.contentMode = .scaleAspectFit
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if self.textField === textField {
      textField.resignFirstResponder()
      return false
    }
    return true
  }
}

