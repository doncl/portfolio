//
//  DiscreteSlidersVC.swift
//  DonClore
//
//  Created by Don Clore on 8/28/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class DiscreteSlidersVC: UIViewController {
  @IBOutlet var slider1: DiscreteSlider!
  @IBOutlet var slider2: DiscreteSlider!
  
  @IBOutlet var slider1Val: UILabel!
  @IBOutlet var slider2Val: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    slider1.values = ["Low", "Medium", "High"]
    slider1.addTarget(self, action: #selector(DiscreteSlidersVC.slider1ValueChanged(_:)),
      for: .valueChanged)
    
    slider2.values = ["Never", "Occasionally", "Sometimes", "Freqently", "Always"]
    slider2.addTarget(self, action: #selector(DiscreteSlidersVC.slider2ValueChanged(_:)),
      for: .valueChanged)
    
    slider1Val.text = slider1.values[0]
    slider2Val.text = slider2.values[0]
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    guard let nav = navigationController else {
      return
    }
    nav.interactivePopGestureRecognizer?.isEnabled = false
  }  
}

//MARK: Behaviors
extension DiscreteSlidersVC {
  @objc func slider1ValueChanged(_ sender: DiscreteSlider) {
    guard sender.discreteValue < sender.values.count else {
      return
    }
    slider1Val.text = sender.values[sender.discreteValue]
  }
  
  @objc func slider2ValueChanged(_ sender: DiscreteSlider) {
    guard sender.discreteValue < sender.values.count else {
      return
    }
    slider2Val.text = sender.values[sender.discreteValue]
  }
}
