//
//  DropDownVC.swift
//  DonClore
//
//  Created by Don Clore on 8/28/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class DropDownVC: UIViewController {

  @IBOutlet var dropDownSingle: BBPDropdown!
  @IBOutlet var dropDownMulti: BBPDropdown!
  
  @IBOutlet var singleDropDownHeightConstraint: NSLayoutConstraint!
  @IBOutlet var multiDropDownHeightConstraint: NSLayoutConstraint!
  
  let data = ["Beatles", "Rolling Stones", "Jimi Hendrix", "King Crimson",
              "Emerson, Lake and Palmer", "Gentle Giant", "Yes", "Jethro Tull", "Genesis",
              "The Grateful Dead", "Jefferson Airplane"]
  
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    dropDownSingle.delegate = self
    dropDownMulti.delegate = self
    dropDownMulti.isMultiple = true
    dropDownSingle.isMultiple = false
    dropDownSingle.data = data
    dropDownMulti.data = data
  }
   
  override func viewDidAppear(_ animated: Bool) {
    dropDownSingle.readjustHeight()
    dropDownMulti.readjustHeight()
  }
}

extension DropDownVC: BBPDropDownDelegate {
  func requestNewHeight(_ dropDown: BBPDropdown, newHeight: CGFloat) {
    UIView.animate(withDuration: 0.6, delay:0.2, options:UIViewAnimationOptions(), animations: {
      if dropDown === self.dropDownMulti {
        self.multiDropDownHeightConstraint.constant = newHeight
      } else {
        self.singleDropDownHeightConstraint.constant = newHeight
      }
      
    }, completion: nil)
  }
  
  func dropDownView(_ dropDown: BBPDropdown, didSelectedItem item: String) {
    // DO nothing for this example.
    print("single select item selected \(item)")
  }
  
  func dropDownView(_ dropDown: BBPDropdown, dataList: [String]) {
    // DO nothing for this example.
    print("multi-select items selected \(dataList)")
  }
  
  func dropDownWillAppear(_ dropDown: BBPDropdown) {
    print("dropdown will appear")
  }
}
