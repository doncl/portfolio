//
//  StarVC.swift
//  DonClore
//
//  Created by Don Clore on 8/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class StarVC: UIViewController {

  @IBOutlet var starImageView: UIImageView!

  @IBOutlet var textField: UITextField!

  override func viewDidLoad() {
    super.viewDidLoad()
    
    textField.delegate = self
  }
}

extension StarVC : UITextFieldDelegate {
  func textFieldDidEndEditing(_ textField: UITextField) {
    guard let text = textField.text else {
      return
    }
    guard let sides = Int(text) else {
      return
    }
    print("sides = \(sides)")
 
    // yes, we could vary the percentInflection with the side count, but I'm not that
    // ambitious.  As it is, this code gets the point across.
    guard let image = pushGetImage(with: { (r : CGRect) in
      guard let b = UIBezierPath.starShape(numberOfInflections: sides,
                                           percentInflection: 8.0) else {
                                            return
      }
      
      b.fit(to: r)
      b.fill(withColor: .blue)
      b.stroke(withWidth: 4.0, andColor: .black)
    }, andSize: view.bounds.size) else {
      return
    }
    
    starImageView.image = image
    starImageView.contentMode = .scaleAspectFit
    
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if self.textField === textField {
      textField.resignFirstResponder()
      return false
    }
    return true
  }
}
