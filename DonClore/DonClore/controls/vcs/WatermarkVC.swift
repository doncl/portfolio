//
//  WatermarkVC.swift
//  DonClore
//
//  Created by Don Clore on 8/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class WatermarkVC: UIViewController {

  @IBOutlet var baoImage: UIImageView!
  

  @IBAction func buttonTouched(_ sender: Any) {
    guard let image = baoImage.image else {
      return
    }
    let imgSize = image.size
    let imgRect = CGRect.from(size: imgSize)
    let imgVueRect = baoImage.bounds
    let targetScale = imgRect.aspectScaleFill(destSize: imgVueRect.size)
    let targetSize = imgRect.size.scale(byFactor: targetScale)
    
    if let font = UIFont(name: "HelveticaNeue", size: 48.0) {
      
      if let w = image.watermarked(withText: "Watermark!",
        targetSize: targetSize, font: font) {
        
        baoImage.image = w
      }
    }
  }
}
