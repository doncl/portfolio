//
//  UIViewExtensions.swift
//  DonClore
//
//  Created by Don Clore on 8/28/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit
import QuartzCore

extension UIView {
  func drawBottomShadow() {
    layer.shadowColor = #colorLiteral(red: 0.9198423028, green: 0.9198423028, blue: 0.9198423028, alpha: 1).cgColor
    layer.masksToBounds = false
    layer.shadowOffset = CGSize(width: 0, height: 2.0)
    layer.shadowRadius = 1.0
    layer.shadowOpacity = 0.8    
  }
}
