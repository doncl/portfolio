//
//  FauxShadowFooter.swift
//  DonClore
//
//  Created by Don Clore on 8/27/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit
import QuartzCore

class FauxShadowFooter: UITableViewCell {
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    initPhase2()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    initPhase2()
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    initPhase2()
  }
  
  fileprivate func initPhase2() {
    selectionStyle = .none
    backgroundColor = #colorLiteral(red: 0.9681890607, green: 0.9681890607, blue: 0.9681890607, alpha: 1)
    contentView.isHidden = true
  }
  
  override func layoutSubviews() {
    layer.shadowColor =  #colorLiteral(red: 0.9198423028, green: 0.9198423028, blue: 0.9198423028, alpha: 1).cgColor
    layer.masksToBounds = false
    layer.shadowOffset = CGSize(width: 0, height: 3.0)
    layer.shadowRadius = 0.8
    layer.shadowOpacity = 0.8
  }
}

