//
//  MenuItem.swift
//  DonClore
//
//  Created by Don Clore on 9/3/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class MenuItem {
  let title : String
  let image : UIImage
  let color : UIColor
  
  
  init(title: String, image: UIImage, color: UIColor) {
    self.title = title
    self.image = image
    self.color = color
  }
  
  class var sharedItems : [MenuItem]  {
    struct Static {
      static let items = MenuItem.sharedMenuItems()
    }
    return Static.items
  }
  
  class func sharedMenuItems() -> [MenuItem] {
    let menuColors = [
      UIColor(red: 249/255, green: 84/255,  blue: 7/255,   alpha: 1.0),
      UIColor(red: 69/255,  green: 59/255,  blue: 55/255,  alpha: 1.0),
      UIColor(red: 249/255, green: 194/255, blue: 7/255,   alpha: 1.0),
      UIColor(red: 32/255,  green: 188/255, blue: 32/255,  alpha: 1.0),
      UIColor(red: 207/255, green: 34/255,  blue: 156/255, alpha: 1.0),
      UIColor(red: 14/255,  green: 88/255,  blue: 149/255, alpha: 1.0),
    ]
    
    
    var items : [MenuItem] = []
    
    items.append(MenuItem(title: "File", image: #imageLiteral(resourceName: "File"), color: menuColors[0]))
    items.append(MenuItem(title: "Edit", image: #imageLiteral(resourceName: "Edit"), color: menuColors[1]))
    items.append(MenuItem(title: "Email", image: #imageLiteral(resourceName: "Email"), color: menuColors[2]))
    items.append(MenuItem(title: "News", image: #imageLiteral(resourceName: "News"), color: menuColors[3]))
    items.append(MenuItem(title: "Print", image: #imageLiteral(resourceName: "Print"), color: menuColors[4]))
    items.append(MenuItem(title: "Fax", image: #imageLiteral(resourceName: "fax"), color: menuColors[5]))
    
    return items
  }

}
