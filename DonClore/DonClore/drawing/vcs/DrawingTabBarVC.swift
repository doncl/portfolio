//
//  DrawingTabBarVC.swift
//  DonClore
//
//  Created by Don Clore on 8/28/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class DrawingTabBarVC: UITabBarController {

  var video : VideoView?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil,
      action: nil)
    
    self.delegate = self
    
    DispatchQueue.main.async {
      self.fireUpVideo(fromURL: VideoURLS.drawingVideo, callback: { videoView in
        self.video = videoView
      })
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    UITabBarItem.appearance()
      .setTitleTextAttributes(
        [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16.0)], for: .normal)
    
    UITabBarItem.appearance()
      .setTitleTextAttributes(
        [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16.0)], for: .selected)
    
    UITabBar.appearance().tintColor = #colorLiteral(red: 0.3179988265, green: 0.3179988265, blue: 0.3179988265, alpha: 1)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    if let video = video {
      video.pause()
    }
    super.viewWillDisappear(animated)
  }
}

extension DrawingTabBarVC : UITabBarControllerDelegate {
  override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    if let video = video {
      video.pause()
    }
  }
}
