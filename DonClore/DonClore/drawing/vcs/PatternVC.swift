//
//  PatternVC.swift
//  DonClore
//
//  Created by Don Clore on 8/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class PatternVC: UIViewController {

  let bubbleGum  = UIColor(red: 1.0, green: 0.400, blue: 1.00, alpha: 1.0)
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    guard let patternImage = buildPattern() else {
      return
    }
    
    view.backgroundColor = UIColor(patternImage: patternImage)
  }
}

//MARK: Behavior
extension PatternVC {

  
  /// This is translated from Erica Sadun's UIKit drawing book from Obj-C to Swift 3.
  ///
  /// - Returns: An image, or nil
  fileprivate func buildPattern() -> UIImage? {
    // Create as small tile
    let targetSize = CGSize(width: 80, height: 80)
    let targetRect = CGRect(origin: .zero, size: targetSize)
    
    // Start a new image
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    guard let context = UIGraphicsGetCurrentContext() else {
      return nil
    }
    defer {
      UIGraphicsEndImageContext()
    }
    
    // Fill background with pink
    bubbleGum.set()
    UIRectFill(targetRect)
    
    // Draw a couple of masks in gray
    UIColor.gray.set()
    
    // First bigger, with interior detail in the top-left
    let littleRect = CGRect(x: 0, y: 0, width: 40, height: 40)
    let mask = buildTheatreMaskPath()
    mask.fit(to: littleRect)
    mask.rotate(angleInRadians: CGFloat.pi / 4)
    mask.fill()
    
    // Then smaller, flipped around, and offset down and right
    mask.rotate(angleInRadians: CGFloat.pi)
    mask.offset(by: CGSize(width: 40.0, height: 40.0))
    mask.scale(sx: 0.5, sy: 0.5)
    mask.fill()
    
    // Retrive and return the pattern image
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    return image
  }
  
  }
