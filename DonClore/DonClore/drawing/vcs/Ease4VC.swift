//
//  Ease4VC.swift
//  DonClore
//
//  Created by Don Clore on 8/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class Ease4VC: UIViewController {
  @IBOutlet var imageView: UIImageView!
  @IBOutlet var exampleTypeLabel: UILabel!

  var exampleIndex : Int = 0
  
  let purple : UIColor = #colorLiteral(red: 0.3882352941, green: 0.2431372549, blue: 0.6352941176, alpha: 1)
  let green : UIColor = #colorLiteral(red: 0.4901960784, green: 0.6352941176, blue: 0.2470588235, alpha: 1)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    exampleIndex = 0
  }
  
  @IBAction func buttonTouched(_ sender: UIButton) {
    buildExample(index: exampleIndex)
    exampleIndex = (exampleIndex + 1) % 4
  }
}

//MARK: Behaviors
extension Ease4VC {
  fileprivate func buildExample(index : Int) {
    let targetSize = CGSize(width: 400.0, height: 400.0)
    
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let targetRect = CGRect(origin: .zero, size: targetSize)
    let inset = targetRect.insetBy(dx: 80.0, dy: 80.0)
    let p = UIBezierPath(roundedRect: inset, cornerRadius: 32.0)
    p.fill(withColor: green)
    p.addClip()
    
    let p0 = inset.pointAtPercents(xPercent: 0.7, yPercent: 0.5)
    let p1 = inset.pointAtPercents(xPercent: 1.0, yPercent: 0.5)
    
    let c1 : UIColor = UIColor(white: 0.0, alpha: 0.0)
    let c2 : UIColor = UIColor(white: 0.0, alpha: 1.0)
    
    
    switch index {
    case 0:
      guard let gradient = Gradient.from(color1: c1, to: c2) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: [])
      exampleTypeLabel.text = "standard linear gradient"
      
    case 1:
      guard let gradient = Gradient.easeIn(between: c1, and: c2) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: [])
      exampleTypeLabel.text = "Gradient easeIn curve"
      
    case 2:
      guard let gradient = Gradient.easeInOut(between: c1, and: c2) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: [])
      exampleTypeLabel.text = "Gradient easeInOut curve"
      
    case 3:
      guard let gradient = Gradient.easeOut(between: c1, and: c2) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: [])
      exampleTypeLabel.text = "Gradient easeOut curve"
      
    default:
      break
    }
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    
    imageView.image = image
  }
}
