//
//  GradientFxVC.swift
//  DonClore
//
//  Created by Don Clore on 8/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class GradientFxVC: UIViewController {
  @IBOutlet var imageView: UIImageView!
  @IBOutlet var effectTypeLabel: UILabel!
  
  var exampleIndex : Int = 0
  
  let purple : UIColor = #colorLiteral(red: 0.3882352941, green: 0.2431372549, blue: 0.6352941176, alpha: 1)
  let green : UIColor = #colorLiteral(red: 0.4901960784, green: 0.6352941176, blue: 0.2470588235, alpha: 1)
  let darkGreen : UIColor = #colorLiteral(red: 0.1568627451, green: 0.2156862745, blue: 0.1254901961, alpha: 1)
  
  override func viewDidLoad() {
    exampleIndex = 0
    
    
  }
  

  @IBAction func buttonTouched(_ sender: UIButton) {
    
    buildFxExample(index: exampleIndex)
    exampleIndex = (exampleIndex + 1) % 7
  }
}

extension GradientFxVC {
  func buildFxExample(index: Int) {
    let targetSize = CGSize(width: 400, height: 200)
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let targetRect = CGRect(origin: .zero, size: targetSize)
    let inset = targetRect.inset(byPercent: 0.15)
    
    switch index {
    case 0:
      UIBezierPath.fill(rect: targetRect, withColor: UIColor.white)
      UIBezierPath.drawStrokedShadowedText(string: "Stroke and Shadow",
        fontFace: "Avenir-BlackOblique", baseColor: green, dest: inset)
        effectTypeLabel.text = "Stroked Shadowed Text"
      
    case 1:
      UIBezierPath.fill(rect: targetRect, withColor: green)
      let path = buildTheatreMaskPath()
      
      path.fit(to: inset)
      path.drawIndented(primary: green, rect: inset)
      effectTypeLabel.text = "Impressed path"
      
    case 2:
      let path = UIBezierPath(roundedRect: inset, cornerRadius: 32)
      path.addClip()
      path.fill(withColor: .black)
      guard let gradient = Gradient.rainbow() else {
        break
      }
      let embossColor : UIColor = UIColor(white: 0.0, alpha: 0.5)
      path.emboss(color: embossColor, radius: 2, blur: 2)
      guard let agate = UIImage(named: "rockFormation.jpg") else {
        break
      }
      path.draw(overTexture: agate, usingGradient: gradient, withAlpha: 0.5)
      effectTypeLabel.text = "Rainbow gradient over rock texture"
      
    case 3:
      let targetColor = darkGreen
      let path = UIBezierPath(roundedRect: inset, cornerRadius: 32)
      path.addClip()
      path.fillWithNoise(fillColor: targetColor)
      guard let gradient = Gradient.linearGloss(color: targetColor) else {
        break
      }
      gradient.drawTopToBottom(rect: inset)
      effectTypeLabel.text = "Noise color fill with linear gloss gradient"
      
    case 4:
      let targetColor = darkGreen
      let path = UIBezierPath(roundedRect: inset, cornerRadius: 32)
      path.fill(withColor: targetColor)
      path.addClip()
      guard let gradient = Gradient.linearGloss(color: targetColor) else {
        break
      }
      gradient.drawTopToBottom(rect: inset)
      
      guard let text = UIBezierPath.from(string: "Button",
        withFontFace: "HelveticaNeue") else {
                                          
        break
      }
      let insetPercent = inset.inset(byPercent: 0.4)
      text.fit(to: insetPercent)
      text.fill(withColor: .white)
      effectTypeLabel.text = "Linear Gloss with text"
      
    case 5:
      let targetColor = darkGreen
      let path = UIBezierPath(roundedRect: inset, cornerRadius: 32)
      path.fill(withColor: targetColor)
      let glow : UIColor = UIColor(white: 0.4, alpha: 1.0)
      path.drawBottomGlow(color: glow, percent: 0.4)
      effectTypeLabel.text = "Bottom Glow"
      
    case 6:
      let targetColor = darkGreen
      let path = UIBezierPath(roundedRect: inset, cornerRadius: 32)
      path.fill(withColor: targetColor)
      
      guard let text = UIBezierPath.from(string: "Button",
                                         withFontFace: "HelveticaNeue") else {
                                          break
      }
      
      let percentInset = inset.inset(byPercent: 0.4)
      text.fit(to: percentInset)
      text.fill(withColor: .white)
      
      let glow : UIColor = UIColor(white: 0.4, alpha: 1.0)
      path.drawBottomGlow(color: glow, percent: 0.4)
      path.drawIconTopLight(p: 0.45)
      effectTypeLabel.text = "Icon top light effect"
      
    default:
      break
    }
   
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    imageView.image = image
    imageView.contentMode = .scaleAspectFit
  }
}
