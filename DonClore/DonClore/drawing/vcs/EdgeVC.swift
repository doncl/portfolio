//
//  EdgeVC.swift
//  DonClore
//
//  Created by Don Clore on 8/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class EdgeVC: UIViewController {
  @IBOutlet var imageView: UIImageView!
  let green : UIColor = #colorLiteral(red: 0.4901960784, green: 0.6352941176, blue: 0.2470588235, alpha: 1)  

  override func viewDidLoad() {
    let targetSize = CGSize(width: 400.0, height: 400.0)
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let targetRect = CGRect(origin: .zero, size: targetSize)
    let inset = targetRect.insetBy(dx: 80.0, dy: 80.0)
    
    let p = UIBezierPath(ovalIn: inset)
    p.fill(withColor: green)
    p.addClip()
    
    let block : InterpolationBlock = {( percent : CGFloat) in
      let skippingPercent : CGFloat = 0.75
      if percent < skippingPercent {
        return 0
      }
      let scaled = (percent - skippingPercent) * (CGFloat(1.0) / (CGFloat(1.0) - skippingPercent))
      return sin(scaled * CGFloat.pi)
    }
    
    let c1 : UIColor = UIColor(white: 0.0, alpha: 0.0)
    let c2 : UIColor = UIColor(white: 0.0, alpha: 1.0)
    
    guard let gradient = Gradient.using(interpolationBlock: block, between: c1, and: c2) else {
      return
    }
    let center = inset.center
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    context.drawRadialGradient(gradient.gradient, startCenter: center, startRadius: 0,
                               endCenter: center, endRadius: inset.width / 2.0, options: [])
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    imageView.image = image
  }

}
