//
//  FlipVC.swift
//  DonClore
//
//  Created by Don Clore on 8/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class FlipVC: UIViewController {
  @IBOutlet var imageView: UIImageView!
  
  let purple : UIColor = #colorLiteral(red: 0.3882352941, green: 0.2431372549, blue: 0.6352941176, alpha: 1)
  let green : UIColor = #colorLiteral(red: 0.4901960784, green: 0.6352941176, blue: 0.2470588235, alpha: 1)
  let darkGreen : UIColor = #colorLiteral(red: 0.1568627451, green: 0.2156862745, blue: 0.1254901961, alpha: 1)
  
  var flipExample : Int = 0


  @IBAction func buttonTouched(_ sender: UIButton) {
    buildFlipExample(flipExample: flipExample)
    flipExample = (flipExample + 1) % 2
  }
}

//MARK: Behaviors
extension FlipVC  {
  
  fileprivate func buildFlipExample(flipExample : Int) {
    let targetSize = CGSize(width: 400, height: 400)
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
  
    let targetRect = CGRect(origin: .zero, size: targetSize)
    let outerRect = targetRect.insetBy(dx: 80, dy: 80)
    let innerRect = outerRect.insetBy(dx: 40, dy: 40)
  
    let outerPath = UIBezierPath(ovalIn: outerRect)
    let innerPath = UIBezierPath(ovalIn: innerRect)
  
    let c1 : UIColor = UIColor(white: 0.66, alpha: 1.0)
    let c2 : UIColor = UIColor(white: 0.33, alpha: 1.0)
  
    guard let gradient = Gradient.from(color1: c1, to: c2) else {
      return
    }
  
    pushDraw(block: {
      outerPath.addClip()
      gradient.drawTopToBottom(rect: outerRect)
    })
  
    pushDraw(block: {
      innerPath.addClip()
      gradient.drawBottomToTop(rect: innerRect)
    })
  
    let shadowColor : UIColor = UIColor(white: 0.0, alpha: 0.5)
    let shadowSize : CGSize = CGSize(width: 0.0, height: 2.0)
    innerPath.drawInnerShadow(color: shadowColor, size: shadowSize, blur: 2.0)
    let setSize : CGSize = CGSize(width: -4.0, height: 4)
    innerPath.setShadow(color: shadowColor, size: setSize, blur: 4.0)
  
    let skyColor : UIColor = UIColor(red: 0.0, green: 0.75, blue: 1.0, alpha: 1.0)
    let darkSkyColor : UIColor = skyColor.scaleBrightness(amount: 0.5)
  
    switch flipExample {
    case 0:
      break
    case 1:
      let insetRect = innerRect.insetBy(dx: 2.0, dy: 2.0)
      let bluePath = UIBezierPath(ovalIn: insetRect)
    
      // Produce an ease-in-out gradient,
      guard let blueGradient =
        Gradient.easeInOut(between: skyColor, and: darkSkyColor) else {
          break
      }
    
      // Draw the radial gradient
      let center = insetRect.center
      let topRight = insetRect.topRight
      let width = center.distance(from: topRight)
    
      pushDraw(block: {
        bluePath.addClip()
        guard let context = UIGraphicsGetCurrentContext() else {
          return
        }
        context.drawRadialGradient(blueGradient.gradient, startCenter: center,
          startRadius: 0, endCenter: center, endRadius: width, options: [])
      })
    default:
      break
    }
  
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    imageView.image = image
  }
}
