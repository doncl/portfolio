//
//  BezierTextVC.swift
//  DonClore
//
//  Created by Don Clore on 8/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class BezierTextVC: UIViewController {

  @IBOutlet var imAbezier: UIImageView!
  
  @IBOutlet var hiMom: UIImageView!
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    guard let image = UIBezierPath.textImage(from: "I'm a Bezier!",
      withFontFace: "HelveticaNeue", pointSize: 32.0,
      renderSize: CGSize(width: 300.0, height: 50.0)) else {
                                              
      return
    }
    
    imAbezier.contentMode = .scaleAspectFit
    imAbezier.image = image
    
    guard let imageHi = UIBezierPath.textImage(from: "Hi, Mom! :)",
      withFontFace: "HelveticaNeue", pointSize: 32.0,
      renderSize: CGSize(width: 300.0, height: 50.0)) else {
                                              
      return
    }
    
    hiMom.contentMode = .scaleAspectFit
    hiMom.image = imageHi
  }


}
