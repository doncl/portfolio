//
//  BeziersVC.swift
//  DonClore
//
//  Created by Don Clore on 8/28/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit


struct CellSegueTuple {
  var cell : DonVCCell
  var seg : String?
  var xib : String? = nil
  
  init(cell : DonVCCell, seg: String? = nil, xib: String? = nil) {
    self.cell = cell
    self.seg = seg
    self.xib = xib
  }
}

class BeziersVC: UIViewController {
  @IBOutlet var table: UITableView!
  
  let cells : [CellSegueTuple] = [
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "patternCellId", text: "Pattern"), seg: "showPatternVCSegue"),
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "textCellId", text: "Turn Text To Bezier"), seg: "bezierTextVCSegue"),
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "watermarkCellId", text: "Watermark on Photo"), seg : "watermarkVCSegue"),
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "polygonCellId", text: "Generalized Polygon routine"), seg: "polygonVCSegue"),
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "starCellId", text: "Be a Star!"),  seg: "starVCSegue"),
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "inflectCellId", text: "Inflect me", shadowed: true, bold: false), seg: "inflectVCSegue"),
  ]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil,
      action: nil)
    
    table.dataSource = self
    table.separatorStyle = .none
    table.delegate = self
    table.estimatedRowHeight = 200.0
    table.rowHeight = UITableViewAutomaticDimension
    view.backgroundColor = #colorLiteral(red: 0.9761723876, green: 0.9761723876, blue: 0.9761723876, alpha: 1)
    table.backgroundColor = view.backgroundColor
    table.reloadData()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationItem.title = "Beziers"
  }
}

extension BeziersVC : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return cells.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellSegueIdPair = cells[indexPath.row]
    return cellSegueIdPair.cell
  }
}

extension BeziersVC : UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let cellSegueIdPair = cells[indexPath.row]
    if let segueId = cellSegueIdPair.seg {
      performSegue(withIdentifier: segueId, sender: self)
    }
  }
}

