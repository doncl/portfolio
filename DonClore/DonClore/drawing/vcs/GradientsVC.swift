//
//  GradientsVC.swift
//  DonClore
//
//  Created by Don Clore on 8/28/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class GradientsVC: UIViewController {
  @IBOutlet var table: UITableView!

  let cells : [CellSegueTuple] = [
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "grad9CellId", text: "9 Gradient Examples"), seg: "showGradient9VCSegue"),
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "ease4CellId", text: "4 different Ease Curves with gradients"), seg : "showEase4VCSegue"),
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "edgeCellId", text: "Edge Gradient Example"), seg: "showEdgeVCSegue"),
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "flipCellId", text: "Flip Gradient Example"),  seg: "showFlipVCSegue"),
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "fxGradientsCellId", text: "Gradient effects", shadowed: true, bold: false), seg: "showGradientFxVCSegue"),
  ]
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil,
      action: nil)
    
    table.dataSource = self
    table.separatorStyle = .none
    table.delegate = self
    table.estimatedRowHeight = 200.0
    table.rowHeight = UITableViewAutomaticDimension
    view.backgroundColor = #colorLiteral(red: 0.9761723876, green: 0.9761723876, blue: 0.9761723876, alpha: 1)
    table.backgroundColor = view.backgroundColor
    table.reloadData()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationItem.title = "Gradients"
  }
}

extension GradientsVC : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cells.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellSegueIdPair = cells[indexPath.row]
    return cellSegueIdPair.cell
  }
}

extension GradientsVC : UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let cellSegueIdPair = cells[indexPath.row]
    if let segueId = cellSegueIdPair.seg {
      performSegue(withIdentifier: segueId, sender: self)
    }
  }
}
