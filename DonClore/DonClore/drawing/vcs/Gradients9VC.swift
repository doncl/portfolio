//
//  Gradients9VC.swift
//  DonClore
//
//  Created by Don Clore on 8/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class Gradients9VC: UIViewController {
  @IBOutlet var imageView: UIImageView!
  @IBOutlet var exampleType: UILabel!
  
  
  var exampleIndex : Int = 0
  
  let purple : UIColor = #colorLiteral(red: 0.3882352941, green: 0.2431372549, blue: 0.6352941176, alpha: 1)
  let green : UIColor = #colorLiteral(red: 0.4901960784, green: 0.6352941176, blue: 0.2470588235, alpha: 1)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    exampleIndex = 0
  }
  
  @IBAction func buttonTouched(_ sender: Any) {
    buildExample(index: exampleIndex)
    exampleIndex = (exampleIndex + 1) % 9
  }
}

//MARK: Behaviors
extension Gradients9VC {
  fileprivate func buildExample(index : Int) {
    let targetSize = CGSize(width: 400.0, height: 400.0)
    
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let targetRect = CGRect(origin: .zero, size: targetSize)
    let inset = targetRect.insetBy(dx: 80.0, dy: 80.0)
    let insetter = inset.insetBy(dx: 100.0, dy: 100.0)
    let p0 : CGPoint = insetter.origin
    let p1 : CGPoint = insetter.bottomRight
    
    switch index {
    case 0:
      guard let gradient = Gradient.rainbow() else {
        return
      }
      gradient.drawLeftToRight(rect: inset)
      exampleType.text = "Rainbow left-to-right"
      
    case 1:
      let c1 = UIColor(white: 1.0, alpha: 1.0)
      let c2 = UIColor(white: 0.0, alpha: 1.0)
      guard let gradient = Gradient.from(color1: c1, to: c2) else {
        break
      }
      gradient.drawLeftToRight(rect: inset)
      exampleType.text = "Grey fade left to right"
      
    case 2:
      let c1 = UIColor(white: 1.0, alpha: 1.0)
      let c2 = UIColor(white: 0.0, alpha: 1.0)
      guard let gradient = Gradient.from(color1: c1, to: c2) else {
        break
      }
      let center = inset.center
      let midRight = inset.midRight
      
      gradient.drawRadial(from: center, to: midRight)
      exampleType.text = "Radial Gradient, from center to midRight"
      
    case 3:
      guard let gradient = Gradient.from(color1: green, to: purple) else {
        break
      }
      gradient.draw(from: p0, to: p1)
      exampleType.text = "Linear Gradient, green to purple"
      
    case 4:
      guard let gradient = Gradient.from(color1: green, to: purple) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: .drawsBeforeStartLocation)
      exampleType.text = "Gradient style .drawsBeforeStartLocation"
      
    case 5:
      guard let gradient = Gradient.from(color1: green, to: purple) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: .drawsAfterEndLocation)
      exampleType.text = "Gradient style .drawsAfterEndLocation"
      
    case 6:
      guard let gradient = Gradient.from(color1: green, to: purple) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: [.drawsBeforeStartLocation, .drawsAfterEndLocation])
      exampleType.text = "Gradient drawsBefore | drawsAfter"
      
    case 7:
      guard let gradient = Gradient.from(color1: green, to: purple) else {
        break
      }
      let radii : CGPoint = CGPoint(x: 50.0, y: 100.0)
      gradient.drawRadial(from: p0, to: p1, radii: radii, style: [])
      exampleType.text = "Radial green-purple radii: [50, 100]"
      
    case 8:
      guard let gradient = Gradient.from(color1: green, to: purple) else {
        break
      }
      let radii = CGPoint(x: 50.0, y: 100.0)
      gradient.drawRadial(from: p0, to: p1, radii: radii,
                          style: [.drawsBeforeStartLocation, .drawsAfterEndLocation])
      exampleType.text = "radii: [50, 100], before | after"
    default:
      break
    }
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    
    imageView.image = image
  }
}
