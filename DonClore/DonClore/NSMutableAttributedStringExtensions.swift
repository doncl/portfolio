//
//  NSMutableAttributedStringExtensions.swift
//  DonClore
//
//  Created by Don Clore on 8/28/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
  func scaleFontSize(multiplier : CGFloat) {
    let range = NSRange(location: 0, length: length)
    enumerateAttribute(NSAttributedStringKey.font, in: range, using: {
      (attr: Any?, range: NSRange, stop : UnsafeMutablePointer<ObjCBool>) -> Void in
      guard let font = attr as? UIFont else {
        return
      }
      let fontDesc = font.fontDescriptor
      let newSize = fontDesc.pointSize * multiplier
      let newFont = font.withSize(newSize)
      addAttribute(NSAttributedStringKey.font, value: newFont, range: range)      
    })
  }
}
