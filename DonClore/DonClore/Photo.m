//
// Created by Don Clore on 10/6/15.
// Copyright (c) 2017 Beer Barrel Poker Studios. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import "Photo.h"
@import Photos;

@interface Photo()
@end

@implementation Photo
@synthesize phAsset;

- (instancetype)initFromPHAsset:(PHAsset *)asset {
    self = [super init];
    if (self) {
        self.phAsset = asset;
    }
    return self;
}


-(NSData *)returnFileImageData
{
    __block NSURL *filePath = nil;
    const NSString *key = @"PHImageFileURLKey";
    PHImageRequestOptions * options = [[PHImageRequestOptions alloc] init];
    options.networkAccessAllowed = NO;
    options.synchronous = YES;
    [[PHImageManager defaultManager]
        requestImageDataForAsset:self.phAsset
                         options:options resultHandler:^(NSData *data, NSString *uti,
                                                         UIImageOrientation orient,
                                                         NSDictionary *info) {
        if ([info objectForKey:key]) {
            filePath = [info objectForKey:key];
        }
     }];
    return [[NSFileManager defaultManager] contentsAtPath:filePath.path];
}

- (void)returnImage:(CGSize)size completion:(void (^)(UIImage *))completion
{
    __block UIImage *image = nil;
    PHImageRequestOptions * options = [[PHImageRequestOptions alloc] init];
    options.networkAccessAllowed = NO;
  options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    [[PHImageManager defaultManager]
            requestImageForAsset:self.phAsset
                      targetSize:size
                     contentMode:PHImageContentModeAspectFill
                         options:options
                   resultHandler:^(UIImage *result, NSDictionary *info){
                       image = result;
                       if (completion) {
                           completion(image);
                       }
                   }];
}

- (void)returnThumbnail:(CGSize)size completion:(void (^)(UIImage *))completion
{
    __block UIImage *thumbnail = nil;
    PHImageRequestOptions * options = [[PHImageRequestOptions alloc] init];
    options.networkAccessAllowed = NO;
    [[PHImageManager defaultManager]
            requestImageForAsset:self.phAsset
                      targetSize:size
                     contentMode:PHImageContentModeAspectFill
                         options:options
                   resultHandler:^(UIImage *result, NSDictionary *info){
                       thumbnail = result;
                       if (completion) {
                           completion(thumbnail);
                       }
                   }];
}

- (NSString *)uniqueId
{
    return self.phAsset.localIdentifier;
}

- (UIImageOrientation)orientation
{
    PHImageRequestOptions * options = [[PHImageRequestOptions alloc] init];
    options.networkAccessAllowed = NO;
    options.synchronous = YES;
    __block UIImageOrientation blockOrientation;
    [[PHImageManager defaultManager]
            requestImageDataForAsset:self.phAsset
                             options:options
                       resultHandler:^(NSData *__nullable imageData,
                               NSString *__nullable dataUTI,
                               UIImageOrientation orientation,
                               NSDictionary *__nullable info){
                           blockOrientation = orientation;
                       }];
    return blockOrientation;
}
@end
