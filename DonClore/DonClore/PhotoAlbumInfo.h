//
// Copyright (c) 2017 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ALAssetsGroup;

// This class is to hold just enough information for the Menu to display UITableViewCells with
// the title of the group/folder/album, the count of images, and a thumbnail of the first
// image.
@interface PhotoAlbumInfo : NSObject
@property (nonatomic, assign) NSUInteger imageCount;
@property (nonatomic, strong) UIImage *firstPhoto;
@end
