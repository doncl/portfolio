//
// Copyright (c) 2017 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class PhotoAlbum;
@class PhotoAlbumInfo;
@class Photo;
@class PHImageRequestOptions;

@interface PhotoLibraryManager : NSObject
@property (nonatomic, strong) PhotoAlbum  *currentAlbum;
@property (nonatomic, strong) NSString *currentAlbumName;
@property (nonatomic, strong)
NSMutableDictionary<NSString *, PhotoAlbumInfo *> *photoAlbumInfos;
-(void)loadAlbumInfos:(CGSize)cellSize;
+(void)checkImagePermissionsWithBlock:(UIViewController *)vc completion:(void (^)(void))completion;
-(PhotoAlbum *)loadCurrentAlbum:(void (^)(PhotoAlbum *))completion;
-(BOOL)currentAlbumIsCameraRollOrAllPhotos;
-(void)getLatestImageFromCameraRoll:(void (^)(Photo *finalPhoto))completion
                             onError:(void (^)(NSError *error))onError;

// Caching
-(void)resetCache;
-(void)  updateCache:(CGRect)bounds
sortedVisibleIndices:(NSArray<NSIndexPath *> *)sortedVisibleIndices
           imageSize:(CGSize)imageSize;
- (void)cachedPhotoAtIndex:(NSIndexPath *)indexPath
                 imageSize:(CGSize)imageSize
                completion:(void (^)(UIImage * result,
                        NSDictionary * info))completion;

- (void)updateCacheAsync:(CGRect)bounds
    sortedVisibleIndices:(NSArray<NSIndexPath *> *)sortedVisibleIndices
               imageSize:(CGSize)imageSize;
+ (PHImageRequestOptions *)getCachedRequestOptions;
@end
