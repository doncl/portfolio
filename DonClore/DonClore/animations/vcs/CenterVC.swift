//
//  CenterVC.swift
//  DonClore
//
//  Created by Don Clore on 9/3/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class CenterVC: UIViewController {

  @IBOutlet var imageView: UIImageView!
  @IBOutlet var label: UILabel!
  
  var menuItem : MenuItem! {
    didSet {
      view.backgroundColor = menuItem.color
      imageView.image = menuItem.image
      label.text = menuItem.title
      imageView.isHidden = false
      label.isHidden = false
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    imageView.isHidden = true
    label.isHidden = true

    menuItem = MenuItem.sharedItems.first!
  }
}
