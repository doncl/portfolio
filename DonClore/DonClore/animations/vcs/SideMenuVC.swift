//
//  SideMenuVC.swift
//  DonClore
//
//  Created by Don Clore on 9/3/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController {
  @IBOutlet var table: UITableView!
  
  var centerVC : CenterVC?

  override func viewDidLoad() {
    super.viewDidLoad()
    table.delegate = self
    table.dataSource = self
    table.reloadData()
  }
}

extension SideMenuVC : UITableViewDataSource {
  func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
    return MenuItem.sharedItems.count
  }
  
  func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for:indexPath)
      as UITableViewCell
    
    let menuItem = MenuItem.sharedItems[indexPath.row]
    cell.contentView.backgroundColor = menuItem.color
    if let imageView = cell.contentView.viewWithTag(5) as? UIImageView {
      imageView.image = menuItem.image
    }
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?  {
    return tableView.dequeueReusableCell(withIdentifier: "HeaderCell")
  }
}

extension SideMenuVC : UITableViewDelegate {
  func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath) {
    table.deselectRow(at: indexPath, animated:true)
    
    guard let centerVC = centerVC, let containerVC = parent as? ContainerVC else {
      return
    }
    centerVC.menuItem = MenuItem.sharedItems[indexPath.row]
    containerVC.toggleSideMenu()
  }
  
  func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath) -> CGFloat {
    return 84.0
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 0.0
  }  
}
