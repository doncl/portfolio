//
//  CATiledLayerVC.swift
//  DonClore
//
//  Created by Don Clore on 9/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class CATiledLayerVC: UIViewController {
  @IBOutlet var scrollView: UIScrollView!
  
  var tiledLayer = CATiledLayer()
  let colors = [
    UIColor(red: 249/255, green: 84/255,  blue: 7/255,   alpha: 1.0),
    UIColor(red: 69/255,  green: 59/255,  blue: 55/255,  alpha: 1.0),
    UIColor(red: 249/255, green: 194/255, blue: 7/255,   alpha: 1.0),
    UIColor(red: 32/255,  green: 188/255, blue: 32/255,  alpha: 1.0),
    UIColor(red: 207/255, green: 34/255,  blue: 156/255, alpha: 1.0),
    UIColor(red: 14/255,  green: 88/255,  blue: 149/255, alpha: 1.0),
    UIColor(red: 15/255,  green: 193/255, blue: 231/255, alpha: 1.0)
  ]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tiledLayer.frame = CGRect(origin: .zero, size: CGSize(width: 4782, height: 5847))
    tiledLayer.delegate = self
    scrollView.layer.addSublayer(tiledLayer)
    scrollView.contentSize = tiledLayer.frame.size
    tiledLayer.contentsScale = UIScreen.main.scale
    tiledLayer.tileSize = CGSize(width: 256.0, height: 256.0)
    tiledLayer.setNeedsDisplay()
  }

  override func viewWillDisappear(_ animated: Bool) {
    tiledLayer.contents = nil
    tiledLayer.delegate = nil
    tiledLayer.removeFromSuperlayer()
    super.viewWillDisappear(animated)
  }
}

//MARK: delegate
extension CATiledLayerVC : CALayerDelegate {
  func draw(_ layer: CALayer, in ctx: CGContext) {
    let bounds = ctx.boundingBoxOfClipPath
    let rand =  Int(arc4random_uniform(UInt32(colors.count)))
    let color = colors[rand]
    UIGraphicsPushContext(ctx)
    ctx.setShouldAntialias(true)
    let path = buildTheatreMaskPath()
    path.fit(to: bounds)
    ctx.addPath(path.cgPath)
    path.fill(withColor: color)
    path.stroke(withWidth: 0.5, andColor: .black)
    UIGraphicsPopContext()
  }
}
