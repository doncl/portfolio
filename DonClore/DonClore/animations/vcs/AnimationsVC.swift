//
//  AnimationsTabBarVC.swift
//  DonClore
//
//  Created by Don Clore on 9/3/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class AnimationsVC: UIViewController {
  @IBOutlet var table: UITableView!
  
  var video : VideoView?
  let cells : [CellSegueTuple] = [
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "uiViewAnimationsCellId",
      text: "UIView Animation"), seg: "showPhantomZoneVCSegue"),
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "layerAnimationsCellId",
      text: "Layer Animation (aka Core Animation)"), seg: "showLayerAnimationsVCSegue"),
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "3DMenuCellId",
      text: "3D Animating Menu", shadowed: true), seg: "show3DMenuVCSegue"),
  ]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil,
      action: nil)
    
    table.dataSource = self
    table.separatorStyle = .none
    table.delegate = self
    table.estimatedRowHeight = 200.0
    table.rowHeight = UITableViewAutomaticDimension
    view.backgroundColor = #colorLiteral(red: 0.9761723876, green: 0.9761723876, blue: 0.9761723876, alpha: 1)
    table.backgroundColor = view.backgroundColor
    table.reloadData()
    
    DispatchQueue.main.async {
      self.fireUpVideo(fromURL: VideoURLS.animationsVideo, callback: { videoView in
        self.video = videoView
      })
    }
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationItem.title = "Animations"
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    if let video = video {
      video.pause()
    }
    super.viewWillDisappear(animated)
  }
}

extension AnimationsVC : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cells.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellSegueIdPair = cells[indexPath.row]
    return cellSegueIdPair.cell
  }
}

extension AnimationsVC : UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let tuple = cells[indexPath.row]
    if let segueId = tuple.seg {
      if let video = video {
        video.pause()
        video.fadeOut()
      }
      performSegue(withIdentifier: segueId, sender: self)
    } 
  }
}
