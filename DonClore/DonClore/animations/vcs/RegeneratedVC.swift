//
//  RegeneratedVC.swift
//  DonClore
//
//  Created by Don Clore on 9/3/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class RegeneratedVC: UIViewController {
  lazy var missy: UIImageView = {
    let missy = UIImageView(image: UIImage(named: "missy")!)
    missy.translatesAutoresizingMaskIntoConstraints = false
    missy.contentMode = .scaleAspectFill
    return missy
  }()


  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.addSubview(missy)
    
    NSLayoutConstraint.activate([
      missy.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      missy.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      missy.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      missy.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    navigationItem.backBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
      target: self, action: #selector(RegeneratedVC.back(_:)))
  }
  
  @objc func back(_ sender: UIBarButtonItem) {
    guard let nav = navigationController else {
      return
    }
    nav.popViewController(animated: true)
  }
}
