//
//  ContainerVC.swift
//  DonClore
//
//  Created by Don Clore on 9/3/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ContainerVC: UIViewController {
  
  var menuVC : SideMenuVC?
  var centerNav : UINavigationController?
  var isOpening = false
  
  let menuWidth : CGFloat = 80.0
  let animationTime : TimeInterval = 0.5

  override func viewDidLoad() {
    super.viewDidLoad()

  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    if let parentNav = navigationController {
      parentNav.interactivePopGestureRecognizer?.isEnabled = false
    }

    let storyboard = UIStoryboard(name: "3DMenu", bundle: nil)
    
    guard let centerNav = storyboard.instantiateViewController(withIdentifier: "CenterNav")
      as? UINavigationController else {
        
        return
    }
    self.centerNav = centerNav
    centerNav.isNavigationBarHidden = true
    
    guard let menuVC = storyboard.instantiateViewController(withIdentifier: "SideMenu")
      as? SideMenuVC else {
        return
    }
    
    self.menuVC = menuVC
    
    guard let centerVC = centerNav.viewControllers.first as? CenterVC else {
      return
    }
    menuVC.centerVC = centerVC
    
    view.backgroundColor = .black
    setNeedsStatusBarAppearanceUpdate()
    
    addChildViewController(centerNav)
    view.addSubview(centerNav.view)
    centerNav.didMove(toParentViewController: self)
    
    addChildViewController(menuVC)
    view.addSubview(menuVC.view)
    menuVC.didMove(toParentViewController: self)
    
    menuVC.view.layer.anchorPoint.x = 1.0
    menuVC.view.frame = CGRect(x: -menuWidth, y: 0, width: menuWidth,
                               height: view.frame.height)
    
    let panGesture = UIPanGestureRecognizer(target:self,
                                            action:#selector(ContainerVC.handleGesture(_:)))
    
    view.addGestureRecognizer(panGesture)
    
    setMenu(toPercent: 0.0)
  }
}

extension ContainerVC {
  @objc func handleGesture(_ recognizer: UIPanGestureRecognizer) {
    guard let centerNav = centerNav, let menuVC = menuVC else {
      return
    }
    
    let translation = recognizer.translation(in: recognizer.view!.superview!)
    
    var progress = translation.x / menuWidth * (isOpening ? 1.0 : -1.0)
    progress = min(max(progress, 0.0), 1.0)
    
    switch recognizer.state {
    case .began:
      let isOpen = floor(centerNav.view.frame.origin.x/menuWidth)
      isOpening = isOpen == 1.0 ? false: true
      // Improve the look of the opening menu
      menuVC.view.layer.shouldRasterize = true
      menuVC.view.layer.rasterizationScale = UIScreen.main.scale
      
    case .changed:
      self.setMenu(toPercent: isOpening ? progress: (1.0 - progress))
      
    case .ended: fallthrough
    case .cancelled: fallthrough
    case .failed:
      
      var targetProgress: CGFloat
      if (isOpening) {
        targetProgress = progress < 0.5 ? 0.0 : 1.0
      } else {
        targetProgress = progress < 0.5 ? 1.0 : 0.0
      }
      
      UIView.animate(withDuration: animationTime, animations: {
        self.setMenu(toPercent: targetProgress)
      }, completion: {_ in
        menuVC.view.layer.shouldRasterize = false
      })
      
    default: break
    }
  }
  
  func toggleSideMenu() {
    guard let centerNav = centerNav, let menuVC = menuVC else {
      return
    }
    let isOpen = floor(centerNav.view.frame.origin.x/menuWidth)
    let targetProgress: CGFloat = isOpen == 1.0 ? 0.0: 1.0
    
    UIView.animate(withDuration: animationTime, animations: {
      self.setMenu(toPercent: targetProgress)
    }, completion: { _ in
      menuVC.view.layer.shouldRasterize = false
    })
  }
  
  func setMenu(toPercent percent: CGFloat) {
    guard let centerNav = centerNav, let menuVC = menuVC else {
      return
    }
    centerNav.view.frame.origin.x = menuWidth * CGFloat(percent)
    menuVC.view.layer.transform = menuTransform(percent: percent)
    menuVC.view.alpha = CGFloat(max(0.2, percent))
  }
  
  func menuTransform(percent: CGFloat) -> CATransform3D {
    var identity = CATransform3DIdentity
    identity.m34 = -1.0/1000
    let remainingPercent = 1.0 - percent
    let angle = remainingPercent * .pi * -0.5
    
    let rotationTransform = CATransform3DRotate(identity, angle, 0.0, 1.0, 0.0)
    let translationTransform = CATransform3DMakeTranslation(menuWidth * percent, 0, 0)
    
    return CATransform3DConcat(rotationTransform, translationTransform)
  }  
}

