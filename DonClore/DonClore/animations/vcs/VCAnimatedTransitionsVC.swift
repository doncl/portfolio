//
//  VCAnimatedTransitionsVC.swift
//  DonClore
//
//  Created by Don Clore on 9/3/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class VCAnimatedTransitionsVC: UIViewController {

  @IBOutlet var slide: AnimatedMaskLabel!
  let transition = PanDrivenTransition()

  override func viewDidLoad() {
    super.viewDidLoad()
    slide.translatesAutoresizingMaskIntoConstraints = false
    
    let pan = UIPanGestureRecognizer(target: self,
      action: #selector(VCAnimatedTransitionsVC.didPan(_:)))
    slide.isUserInteractionEnabled = true
    slide.addGestureRecognizer(pan)
    
    guard let nav = navigationController else {
      return
    }
    nav.delegate = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    guard let nav = navigationController else {
      return
    }
    nav.interactivePopGestureRecognizer?.isEnabled = false
  }
  
  @objc func didPan(_ recognizer: UIPanGestureRecognizer) {
    switch recognizer.state {
    case .began:
        transition.interactive = true
        if let nav = navigationController {
            let vc = RegeneratedVC()
            nav.pushViewController(vc, animated: true)
        }
       //performSegue(withIdentifier: "regenerateSegue", sender: nil)
    default:
      transition.handlePan(recognizer: recognizer)
    }
  }
}

extension VCAnimatedTransitionsVC: UINavigationControllerDelegate {
  func navigationController(_ navigationController: UINavigationController,
    animationControllerFor operation: UINavigationControllerOperation,
    from fromVC: UIViewController, to toVC: UIViewController)
    -> UIViewControllerAnimatedTransitioning? {
      
    transition.operation = operation
    return transition
  }
  
  func navigationController(_ navigationController: UINavigationController,
                            interactionControllerFor animationController: UIViewControllerAnimatedTransitioning)
    -> UIViewControllerInteractiveTransitioning? {
      
      
      if false == transition.interactive {
        return nil
      }
      return transition
  }
}

