//
//  LayerAnimationsVC.swift
//  DonClore
//
//  Created by Don Clore on 9/3/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class LayerAnimationsVC: UIViewController {
  @IBOutlet var table: UITableView!

  
  let cells : [CellSegueTuple] = [
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "vcTransitionCellId",
      text: "View Controller animated transition"),
      seg: "showVCAnimatedTransitionsVC"),
    CellSegueTuple(cell: DonVCCell(style: .default, reuseIdentifier: "tileLayerCellId",
      text: "CATiledLayer example", shadowed: true), seg: "showCATiledLayerVCSegue"),
  ]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil,
                                                       action: nil)
    
    table.dataSource = self
    table.separatorStyle = .none
    table.delegate = self
    table.estimatedRowHeight = 200.0
    table.rowHeight = UITableViewAutomaticDimension
    view.backgroundColor = #colorLiteral(red: 0.9761723876, green: 0.9761723876, blue: 0.9761723876, alpha: 1)
    table.backgroundColor = view.backgroundColor
    table.reloadData()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationItem.title = "Layer Animation"
  }
}

extension LayerAnimationsVC : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cells.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellSegueIdPair = cells[indexPath.row]
    return cellSegueIdPair.cell
  }
}

extension LayerAnimationsVC : UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let cellSegueIdPair = cells[indexPath.row]
    if let segueId = cellSegueIdPair.seg {
      performSegue(withIdentifier: segueId, sender: self)
    }
  }
}



