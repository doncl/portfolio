//
// Created by Don Clore on 10/6/15.
// Copyright (c) 2017 Beer Barrel Poker Studios. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PhotoLibraryManager.h"

@class ALAsset;
@class PHAsset;

@interface Photo : NSObject
@property (nonatomic, readonly) NSString *uniqueId;
@property (nonatomic, readonly) UIImageOrientation orientation;

// Only on iOS 8 and above.
@property (nonatomic, strong) PHAsset *phAsset;

-(void)returnImage:(CGSize)size completion:(void (^)(UIImage *))completion;
-(void)returnThumbnail:(CGSize)size completion:(void (^)(UIImage *))completion;
-(NSData *)returnFileImageData;

// used on iOS 8 and above.
- (instancetype)initFromPHAsset:(PHAsset *)asset;
@end
