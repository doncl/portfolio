//
//  AppDelegate.swift
//  DonClore
//
//  Created by Don Clore on 8/26/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
 
  func application(_ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
    UINavigationBar.appearance().tintColor = .white
    return true
  }
}

