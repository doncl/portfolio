//
//  VideoURLs.swift
//  DonClore
//
//  Created by Don Clore on 10/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct VideoURLS {
  static let introVideo =
  URL(string: "https://doncl-transcoded-video.s3.amazonaws.com/IntroGeneric/IntroGeneric.m3u8")!
  
  static let resumeVideo =
  URL(string: "https://doncl-transcoded-video.s3.amazonaws.com/Resume/Resume.m3u8")!
  
  static let interviewingVideo =
  URL(string: "https://doncl-transcoded-video.s3.amazonaws.com/Interviewing/Interviewing.m3u8")!
  
  static let controlsVideo =
  URL(string: "https://doncl-transcoded-video.s3.amazonaws.com/Controls/Controls.m3u8")!
    
  static let drawingVideo =
  URL(string: "https://doncl-transcoded-video.s3.amazonaws.com/Drawing/Drawing.m3u8")!
  
  static let animationsVideo =
  URL(string: "https://doncl-transcoded-video.s3.amazonaws.com/Animations/Animations.m3u8")!
}
