//
//  CGSizeExtensions.swift
//  DrawingUtilities
//
//  Created by Don Clore on 6/30/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.

import UIKit

extension CGSize {
  func scale(byFactor factor : CGFloat) -> CGSize {
    return CGSize(width: width * factor, height: height * factor)
  }
}
