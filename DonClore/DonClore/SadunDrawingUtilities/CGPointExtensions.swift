//
//  CGPointExtensions.swift
//  DrawingUtilities
//
//  Created by Don Clore on 7/2/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.

import UIKit
import QuartzCore

extension CGPoint {
  var isNil: Bool {
    return equalTo(CGPoint.Nil)
  }
  
  static var Nil : CGPoint {
    return CGRect.null.origin
  }
  
  
  /// Returns the point ands slope along a line at a given percentage of the way.
  ///
  /// - Parameters:
  ///   - destPoint: destinationPoint for line.
  ///   - percent: How far along the line have we traversed.
  /// - Returns: A tuple - tuple.0 is the poiunt, tuple.1 is the slope expressed
  ///     as a CGPoint, with dx and dy.   To get the slop as a single float,
  ///     divide dy / dx.
  func interpolateLineSegment(to destPoint : CGPoint, percent : CGFloat)
    -> (CGPoint, CGPoint) {
    
    let dx = destPoint.x - x
    let dy = destPoint.y - y
    let slope = CGPoint(x: dx, y: dy)
      
    let px = x + dx * percent
    let py = y + dy * percent
      
    let interpolatedPoint = CGPoint(x: px, y: py)
    return (interpolatedPoint, slope)
  }
  
  // N.B. The following two methods expect progress between 0.0 and 1.0!!
  
  
  func cubicBezierPoint(progress : CGFloat, controlPoint1 : CGPoint,
    controlPoint2 : CGPoint, end: CGPoint) -> CGPoint {
    
    var point : CGPoint = .zero
    point.x = CGPoint.cubicBezier(progress: progress, startValue: x,
      controlValue1: controlPoint1.x, controlValue2: controlPoint2.x,
      endValue: end.x)
    
    point.y = CGPoint.cubicBezier(progress: progress, startValue: y,
      controlValue1: controlPoint1.y, controlValue2: controlPoint2.y,
      endValue: end.y)
    
    return point
  }
  
  func quadBezierPoint(progress: CGFloat, controlPoint1 : CGPoint, end: CGPoint)
    -> CGPoint {
      
    var point : CGPoint = .zero
    point.x = CGPoint.quadBezier(progress: progress, startValue: x,
      controlValue1: controlPoint1.x, endValue: end.x)
    
    point.y = CGPoint.quadBezier(progress: progress, startValue: y,
      controlValue1: controlPoint1.y, endValue: end.y)
    
    return point
  }
  
  static func cubicBezier(progress : CGFloat, startValue : CGFloat,
    controlValue1 : CGFloat, controlValue2 : CGFloat, endValue: CGFloat)
    -> CGFloat {
    
    let remainder = (1.0 - progress)
    let remainderSquared = remainder * remainder
    let remainderCubed = remainderSquared * remainder
    let progressSquared = progress * progress
    let progressCubed = progressSquared * progress
    
    let result = startValue * remainderCubed
      + (3.0 * controlValue1 * remainderSquared * progress)
      + (3.0 * controlValue2 * remainder * progressSquared)
      + (endValue * progressCubed)
    
    return result
  }
  
  static func quadBezier(progress : CGFloat, startValue: CGFloat,
    controlValue1 : CGFloat, endValue : CGFloat) -> CGFloat {
    
    let remainder = (1.0 - progress)
    let remainderSquared = remainder * remainder
    let progressSquared = progress * progress
    
    let result = startValue * remainderSquared
      + 2.0 * controlValue1 * remainder * progress
      + endValue * progressSquared
    
    return result
  }
  
  //MARK:  Element distance
  func distance(from destPoint: CGPoint) -> CGFloat {
    let dx = destPoint.x - x
    let dy = destPoint.y - y
    return sqrt(dx * dx + dy * dy)
  }
  
  // N.B. - these two could be factored into a single method that takes a 
  // a closure, one for cubic, and one for quad beziers; they're otherwise
  // identical (well, plus the cubic one takes an extra controlPoint, which
  // could be defaulted to nil.   Perhaps they should be; not quite ready to 
  // do that.
  func cubicBezierLength(controlPoint1 : CGPoint, controlPoint2 : CGPoint,
    endPoint : CGPoint) -> CGFloat {
    
    let steps = 6  // number of bezier samples - this is a tradeoff thing
    
    var current : CGPoint = self
    var previous : CGPoint = self
    var length : CGFloat = 0.0
    
    let fSteps : CGFloat = CGFloat(steps)
    
    for i in 0..<steps {
      let progress : CGFloat = CGFloat(i) / fSteps
      current = self.cubicBezierPoint(progress: progress,
        controlPoint1: controlPoint1, controlPoint2: controlPoint2,
        end: endPoint)
      
      if i > 0 {
        length += current.distance(from: previous)
        previous = current
      }
    }
    return length
  }
  
  // quadLength 
  func quadBezierLength(controlPoint1 : CGPoint, endPoint : CGPoint) -> CGFloat {
    
    let steps = 6 // number of bezier samples - this is a tradeoff thing
    
    var current : CGPoint = self
    var previous : CGPoint = self
    var length : CGFloat = 0.0
    
    let fSteps : CGFloat = CGFloat(steps)
    
    for i in 0..<steps {
      let progress : CGFloat = CGFloat(i) / fSteps
      current = self.quadBezierPoint(progress: progress,
        controlPoint1: controlPoint1, end: endPoint)
      
      if i > 0 {
        length += current.distance(from: previous)
        previous = current
      }
    }
    return length
  }
}

//MARK:Clamping
extension CGPoint {
  func clamp(to rect: CGRect) -> CGPoint {
    let clampedX = x.clamp(min: rect.minX, max: rect.maxX)
    let clampedY = y.clamp(min: rect.minY, max: rect.maxY)
    return CGPoint(x: clampedX, y: clampedY)
  }
}


