//
//  CGFloatExtensions.swift
//  DrawingUtilities
//
//  Created by Don Clore on 6/30/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.

import UIKit

//MARK: Angles
extension CGFloat {
  func toRadians() -> CGFloat {
    return self * 180.0 / CGFloat.pi
  }
  
  func toDegrees() -> CGFloat {
    return self * CGFloat.pi / 180.0
  }
}

//MARK: Clamping
extension CGFloat {
  func clamp(min minClampingValue: CGFloat, max maxClampingValue: CGFloat) -> CGFloat {
    let maxOfTwo = fmax(minClampingValue, self)
    let minOfThat = fmin(maxOfTwo, maxClampingValue)
    return minOfThat
  }
}
