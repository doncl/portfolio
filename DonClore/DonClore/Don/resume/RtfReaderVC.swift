//
//  RtfReaderVC.swift
//  DonClore
//
//  Created by Don Clore on 8/27/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class RtfReaderVC: UIViewController {
  
  @IBOutlet var zoomSlider: DiscreteSlider!
  @IBOutlet var textView: UITextView!
  
  var resumeText : NSAttributedString?
  var videoURL : URL?
  var video : VideoView?
  
  let zoomValues : [CGFloat] = [1.0, 1.25, 1.5, 1.75, 2.0]
  var inverseValues : [CGFloat] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    textView.layer.borderWidth = 1.0
    textView.layer.borderColor = UIColor(white: 197.0 / 255.0, alpha: 1.0).cgColor
    
    textView.isEditable = false
    view.backgroundColor = .white
    zoomSlider.values = ["1x", "1.25x", "1.5x", "1.75x", "2x"]
    zoomSlider.addTarget(self, action: #selector(RtfReaderVC.sliderChanged(_:)),
      for: .valueChanged)
    
    if let videoURL = videoURL {
      fireUpVideo(fromURL: videoURL, callback: { videoView in
        self.video = videoView
      })
    }
  }
  
  func loadRtfResource(name: String = "Don_Clore_Resume") {
    
    if let rtfPath = Bundle.main.url(forResource: name, withExtension: "rtf") {
      do {
        let attrStr = try NSAttributedString(url: rtfPath,
          options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.rtf],
          documentAttributes: nil)
        resumeText = attrStr
        } catch let error {
            simpleErrorMessage(error.localizedDescription)
        }
      }
   }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    textView.scrollsToTop = true
    guard let resumeText = resumeText else {
      return 
    }
    if let nav = navigationController?.navigationBar {
      nav.tintColor = .white
    }
    DispatchQueue.main.async {
      self.textView.attributedText = resumeText
      self.textView.contentOffset = CGPoint.zero
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    video?.pause()
    video?.fadeOut()
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    guard let nav = navigationController else {
      return
    }
    nav.interactivePopGestureRecognizer?.isEnabled = false
  }
}

//MARK: Behaviors
extension RtfReaderVC {
  @objc func sliderChanged(_ sender: DiscreteSlider) {
    let oldVal = sender.lastDiscreteValue
    let newVal = sender.discreteValue
    let oldScale = zoomValues[oldVal]
    let newScale = zoomValues[newVal]
    let delta : CGFloat = newScale / oldScale
    let attrString = NSMutableAttributedString(attributedString: textView.attributedText)
    attrString.scaleFontSize(multiplier: delta)
    textView.attributedText = attrString
    textView.setNeedsDisplay()
  }
}
