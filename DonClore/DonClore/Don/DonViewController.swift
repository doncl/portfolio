//
//  ViewController.swift
//  DonClore
//
//  Created by Don Clore on 8/26/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class DonViewController: UIViewController {

  @IBOutlet var table: UITableView!
  
  var video : VideoView?
  
  let donResumeCell = DonVCCell(style: .default, reuseIdentifier: "donResumeCellId",
    shadowed: false, bold: true)
  
  let interviewCell = DonVCCell(style: .default, reuseIdentifier: "interViewCellId",
    shadowed: true, bold: false)
  
  let controlsCell = DonVCCell(style: .default, reuseIdentifier: "controlsCellId")
  let drawingCell = DonVCCell(style: .default, reuseIdentifier: "drawingCellId")
  let animCell = DonVCCell(style: .default, reuseIdentifier : "animationCellId", shadowed: true)
  
  let fauxFooter = FauxShadowFooter(style: .default, reuseIdentifier: "fauxFooterCellId")
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil,
                                                       action: nil)
    
    let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200.0, height: 25.0))
    titleLabel.text = "Don Clore"
    titleLabel.textColor = .white
    titleLabel.textAlignment = .center
    titleLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
    navigationItem.titleView = titleLabel
    donResumeCell.labelText = "Resume"
    interviewCell.labelText = "Thoughts on Interviewing"
    controlsCell.labelText = "Controls"
    drawingCell.labelText = "Drawing"
    animCell.labelText = "Animations"
    view.backgroundColor = #colorLiteral(red: 0.9761723876, green: 0.9761723876, blue: 0.9761723876, alpha: 1)
    table.backgroundColor = view.backgroundColor
    table.delegate = self
    table.dataSource = self
    table.rowHeight = UITableViewAutomaticDimension
    table.estimatedRowHeight = 200.0
    
    if let label = donResumeCell.detailTextLabel {
      label.text = "Don Clore Resume"
    }
    table.reloadData()
    
    DispatchQueue.main.async {
      self.fireUpVideo(fromURL: VideoURLS.introVideo, callback: { videoView in
        self.video = videoView
      })
    }
  }  
}

extension DonViewController : UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.section == 1 {
      return 10.0
    }
    return UITableViewAutomaticDimension
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return 2
    } else if section == 1 {
      return 1
    } else if section == 2 {
      return 3
    }
    return 0
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.section == 0 {
      if indexPath.row == 0 {
        return donResumeCell
      }
      return interviewCell
    } else if indexPath.section == 1 {
      return fauxFooter
    } else if indexPath.section == 2 {
      if indexPath.row == 0 {
        return controlsCell
      } else if indexPath.row == 1 {
        return drawingCell
      } else if indexPath.row == 2 {
        return animCell
      }
    }
    return UITableViewCell()
  }
}

extension DonViewController : UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let section = indexPath.section
    let row = indexPath.row
    
    if section == 0 {
      if row == 0 {
        performSegue(withIdentifier: "showResumeSegue", sender: self)
      } else if row == 1 {
        performSegue(withIdentifier: "showInterviewSegue", sender: self)
      }
    } else if section == 2 {
      if row == 0 {
        performSegue(withIdentifier: "showControlsTabVCSegue", sender: self)
      } else if row == 1 {
        performSegue(withIdentifier: "showDrawingTabVCSegue", sender: self)
      } else if row == 2 {
        performSegue(withIdentifier: "showAnimationsVCSegue", sender: self)
      }
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    guard let id = segue.identifier else {
      return
    }
    video?.pause()
    video?.fadeOut()
    if id == "showResumeSegue" {
      if let resumeVC = segue.destination as? RtfReaderVC {
        resumeVC.videoURL = VideoURLS.resumeVideo
        resumeVC.loadRtfResource(name: "Don_Clore_Resume")
      }
    } else if id == "showInterviewSegue" {
      if let interviewVC = segue.destination as? RtfReaderVC {
        interviewVC.videoURL = VideoURLS.interviewingVideo
        interviewVC.loadRtfResource(name: "Interviewing")
      }
    } else if id == "showControlsTabVCSegue" {
      if let _ = segue.destination as? ControlsTabBarVC {
        print("It's a controls tab bar vc")
      }
    } else if id == "showDrawingTabVCSegue" {
      print("It's a drawing tabbar vc")
    }
  }
}

