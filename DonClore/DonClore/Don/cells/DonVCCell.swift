//
//  DonVCCell.swift
//  DonClore
//
//  Created by Don Clore on 8/27/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class DonVCCell : UITableViewCell {
  var isShadowed : Bool
  var isBold : Bool
  
  let label : UILabel = UILabel()

  init(style: UITableViewCellStyle, reuseIdentifier: String?, text: String? = nil,
    shadowed : Bool = false, bold : Bool = false) {
    self.isBold = bold
    self.isShadowed = shadowed
    super.init(style: style, reuseIdentifier : reuseIdentifier)
    selectionStyle = .none
    isUserInteractionEnabled = true
    labelText = text
    initPhase2()
  }
  

  
  required init?(coder aDecoder: NSCoder) {
    fatalError("Not supported")
  }
  
  override func awakeFromNib() {
    fatalError("Not supported")
  }
  
  var labelText : String? {
    get {
      return label.text
    }
    set {
      label.text = newValue
    }
  }

  fileprivate func initPhase2() {
    if isShadowed {
      drawBottomShadow()
    }
    autoresizesSubviews = false
    contentView.isHidden = true
    label.translatesAutoresizingMaskIntoConstraints = false
    addSubview(label)
    

    if isBold {
      label.font = UIFont.preferredBoldFont(forTextStyle: .headline)
    } else {
      label.font = UIFont.preferredFont(forTextStyle: .subheadline)
    }
 
    label.textAlignment = .center
    
    NSLayoutConstraint.activate ([
       label.centerXAnchor.constraint(equalTo: centerXAnchor),
       label.centerYAnchor.constraint(equalTo: centerYAnchor),
    ])
  }
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)
    
    if isShadowed {
      return
    }
 
    self.drawLowerBorder(rect, color: #colorLiteral(red: 0.9441131949, green: 0.9441131949, blue: 0.9441131949, alpha: 1), width: 1.0)
  }
}
