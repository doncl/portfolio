Purpose: 
=========

The raison d’etre here is to provide recruiters and potential co-workers a chance to review my work, to understand where I stand as an iOS developer, outside of the confusing, stressful, and often misleading environment of a white-board interview.

Structure:
==========

This is a simple Universal iOS app, with hierarchical navigation structure, that highlights various tools in my iOS toolbox/arsenal. It uses contextual videos to explain, and hopefully illuminate my thinking as to the rationale for the various areas of the app.

System Requirements:
====================

A Mac running Xcode 8, and preferably an iOS target device running iOS 10.

To Install:
===========

Simply clone the repo, and build with Xcode 8. I’ll update it for Xcode 9 / Swift 4 when iOS 11. Obviously, most people using this repo primarily want to read the source code, not build and install, but that’s an option, always.
